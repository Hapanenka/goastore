/*
package by.andrey.springcorse.ShopApp.validators;

import by.andrey.springcorse.ShopApp.config.TestConfig;
import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.repositories.PeopleRepository;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.validation.Errors;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class ValidatorPersonTest {

    @Autowired
    private ValidatorPerson validatorPerson;

    @Autowired
    private PeopleRepository peopleRepository;

    private static final String personEmail = "tes777t@mail.com";

    private static final Person person = mock(Person.class);

    @BeforeAll
    public static void setup() {
        when(person.getEmail()).thenReturn(personEmail);
    }

    @Test
    public void validate(){
        when(peopleRepository.findByEmail(personEmail)).thenReturn(null);
        Errors errors = mock(Errors.class);
        validatorPerson.validate(person, errors);
        verify(errors, never()).rejectValue(eq("email"), any(), any());
    }

    @Test
    public void validateShouldRejectUserWithAlreadyUsedEmail() {
        when(peopleRepository.findByEmail(personEmail)).thenReturn(Optional.ofNullable(person));
        Errors errors = mock(Errors.class);
        validatorPerson.validate(person, errors);
        verify(errors, times(1))
                .rejectValue(eq("email"), any(), any());
    }
}
*/
