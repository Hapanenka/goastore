package by.andrey.springcorse.ShopApp.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {                   //needed start program

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testController() throws Exception {
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void loginTest() throws Exception {
        this.mockMvc.perform(get("/admin/order"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/auth/login"));
    }

    /*@Test
    public void authUserTest() throws Exception {
        this.mockMvc.perform(formLogin().user("tes1t@mail.com").password("123"))
                .andDo(print())
                .andExpect(status().
                .andExpect(redirectedUrl("http://localhost/auth/login"));
    }*/

    @Test
    public void badCredentials() throws Exception {
        this.mockMvc.perform(post("/login").param("user", "user"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

}
