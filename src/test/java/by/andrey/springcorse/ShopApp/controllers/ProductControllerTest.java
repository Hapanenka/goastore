package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.dto.ProductDTO;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {                //needed start program

    @Autowired
    private MockMvc mockMvc;

    RestTemplate restTemplate = new TestRestTemplate().getRestTemplate();

    @Autowired
    private ProductController controller;

    @Test
    public void index() {
        ResponseEntity<List<ProductDTO>> responseEntity = restTemplate.exchange("http://localhost:8091/products",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductDTO>>() {
                });
        List<ProductDTO> list = responseEntity.getBody();
        assertThat(list.size(), is(12));
        List<Integer> ids = list.stream().map(ProductDTO::getIdProduct).toList();
        assertThat(ids, containsInAnyOrder(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    }

    @Test
    public void checkRandomProd() {
        ResponseEntity<List<ProductDTO>> responseEntity = restTemplate.exchange("http://localhost:8091/products/random/7",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductDTO>>() {
                });
        List<ProductDTO> list = responseEntity.getBody();
        assert list != null;
        assertThat(list.size(), is(7));
    }
}