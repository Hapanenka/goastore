package by.andrey.springcorse.ShopApp.dto;

import by.andrey.springcorse.ShopApp.models.ProductSubGroup;
import by.andrey.springcorse.ShopApp.models.ProductSubSubGroup;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProductSubGroupDtoService {

    private final ProductSubSubGroupDtoService productSubSubGroupDtoService;

    @Autowired
    public ProductSubGroupDtoService(ProductSubSubGroupDtoService productSubSubGroupDtoService) {
        this.productSubSubGroupDtoService = productSubSubGroupDtoService;
    }

    public ProductSubGroupDto convertToProductSubGroupDto(ProductSubGroup productSubGroup, String language) {
        ProductSubGroupDto productSubGroupDto = new ProductSubGroupDto();
        productSubGroupDto.setIdSubGroup(productSubGroup.getIdProductSubGroup());
        if (language.equals("ru")) {
            productSubGroupDto.setNameSubGroup(productSubGroup.getProductSubGroupRu());
        } else productSubGroupDto.setNameSubGroup(productSubGroup.getProductSubGroup());
        productSubGroupDto.setSeoSubGroup(productSubGroupDto.getSeoSubGroup());
        if (productSubGroup.getImagesub() != null) {
            productSubGroupDto.setUrlSubGroup(productSubGroup.getImagesub().getOriginalFileName());
        }
        if (productSubGroup.getProductSubSubGroups() != null) {
            productSubGroupDto.setSubSubGroups(
                    productSubGroup.getProductSubSubGroups()
                            .stream()
                            .map(i -> productSubSubGroupDtoService.convertToProductSubSubGroupDto(i, language))
                            .collect(Collectors.toList()));
        }
        return productSubGroupDto;
    }
}