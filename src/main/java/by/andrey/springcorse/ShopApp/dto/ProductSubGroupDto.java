package by.andrey.springcorse.ShopApp.dto;

import by.andrey.springcorse.ShopApp.models.ProductSubGroup;
import lombok.Data;

import java.util.List;

@Data
public class ProductSubGroupDto {

    private long idSubGroup;

    private String nameSubGroup;

    private String urlSubGroup;

    private String seoSubGroup;

    private List<ProductSubSubGroupDto> subSubGroups;
}