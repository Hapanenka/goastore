package by.andrey.springcorse.ShopApp.dto;

import by.andrey.springcorse.ShopApp.models.Image;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageDTOService {

    private final ModelMapper modelMapper;

    @Autowired
    public ImageDTOService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Image convertToImage(ImageDTO imageDTO) {
        return modelMapper.map(imageDTO, Image.class);
    }

    public ImageDTO convertToImageDTO(Image image) {
        return modelMapper.map(image, ImageDTO.class);
    }
}