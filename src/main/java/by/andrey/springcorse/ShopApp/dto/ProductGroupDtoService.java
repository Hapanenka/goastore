package by.andrey.springcorse.ShopApp.dto;

import by.andrey.springcorse.ShopApp.models.ProductGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProductGroupDtoService {

    private final ProductSubGroupDtoService productSubGroupDtoService;

    @Autowired
    public ProductGroupDtoService(ProductSubGroupDtoService productSubGroupDtoService) {
        this.productSubGroupDtoService = productSubGroupDtoService;
    }

    public ProductGroup changePGDtoPG(ProductGroupDto productGroupDto) {
        ProductGroup productGroup = new ProductGroup();
        productGroup.setIdProductGroup(productGroupDto.getIdGroup());
        productGroup.setProductGroup(productGroupDto.getNameGroup());
        productGroup.setSeo((productGroupDto.getSeoGroup()));
        return productGroup;
    }

    public ProductGroupDto changePgToPgDTO(ProductGroup productGroup, String language) {
        ProductGroupDto productGroupDto = new ProductGroupDto();
        productGroupDto.setIdGroup(productGroup.getIdProductGroup());
        if (language.equals("ru")) {
            productGroupDto.setNameGroup(productGroup.getProductGroupRu());
        } else {
            productGroupDto.setNameGroup(productGroup.getProductGroup());
        }
        productGroupDto.setSeoGroup(productGroup.getSeo());
        if (productGroup.getImage() != null) {
            productGroupDto.setUrlGroup(productGroup.getImage().getOriginalFileName());
        }
        if (productGroup.getProductSubGroups() != null) {
            productGroupDto.setSubGroups(
                    productGroup
                            .getProductSubGroups()
                            .stream()
                            .map(i -> productSubGroupDtoService.convertToProductSubGroupDto(i, language))
                            .collect(Collectors.toList()));
        }
        return productGroupDto;
    }
}