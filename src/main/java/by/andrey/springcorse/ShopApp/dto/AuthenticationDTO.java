package by.andrey.springcorse.ShopApp.dto;

import lombok.Data;

@Data
public class AuthenticationDTO {

    private String userName;

    private String password;
}