package by.andrey.springcorse.ShopApp.dto;

import lombok.Data;

@Data
public class ProductSubSubGroupDto {

    private long idSubSubGroup;

    private String nameSubSubGroup;

    private String urlSubSubGroup;

    private String seoSubSubGroup;
}