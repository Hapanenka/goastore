package by.andrey.springcorse.ShopApp.dto;

import lombok.Data;
import java.util.List;

@Data
public class ProductGroupDto {

    private long idGroup;

    private String nameGroup;

    private String urlGroup;

    private String seoGroup;

    private List<ProductSubGroupDto> subGroups;
}