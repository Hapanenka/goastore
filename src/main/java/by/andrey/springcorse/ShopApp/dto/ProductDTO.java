package by.andrey.springcorse.ShopApp.dto;

import by.andrey.springcorse.ShopApp.models.*;
import com.amazonaws.services.dynamodbv2.xspec.S;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProductDTO {

    private long idProduct;

    private String productGroup;

    private String productGroupRu;

    private String productSubGroup;

    private String productSubGroupRu;

    private String productSubSubGroup;

    private String productSubSubGroupRu;

    private String productName;

    private String productNameRu;

    private String makerName;

    private String countryMaker;

    private double priceWithOutDiscount;

    private double productDiscount;

    private double priceOutComing;

    private String specificationColor;

    private List<String> originalFileName;

    private Double currentRating;

    private Integer numberOfRating;
}