package by.andrey.springcorse.ShopApp.dto;


import by.andrey.springcorse.ShopApp.models.PersonRole;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class PersonDTO {                //слоя между сущностью и контроллером

    private long id;

    @NotEmpty(message = "Not Null")
    @Size(min = 3, max = 20)
    private String userName;

    private String firstName;

    private String lastName;

    @Email
    private String email;

    private int age;

    private String city;

    private String area;

    private int postalCode;

    private String country;

    @Size(min = 3, max = 100, message = "Your pass must be in the range of 3 to 100 characters")
    @NotEmpty(message = "Not Null")
    private String password;

    private LocalDateTime registration;

    private String phone;

    private PersonRole role;
}