package by.andrey.springcorse.ShopApp.dto;

import by.andrey.springcorse.ShopApp.models.ProductSubSubGroup;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductSubSubGroupDtoService {

    public ProductSubSubGroupDto convertToProductSubSubGroupDto(ProductSubSubGroup productSubSubGroup, String language) {
        ProductSubSubGroupDto productSubSubGroupDto = new ProductSubSubGroupDto();
        productSubSubGroupDto.setIdSubSubGroup(productSubSubGroup.getIdProductSubSubGroup());
        if (language.equals("ru")) {
            productSubSubGroupDto.setNameSubSubGroup(productSubSubGroup.getProductSubSubGroupRu());
        } else productSubSubGroupDto.setNameSubSubGroup(productSubSubGroup.getProductSubSubGroup());
        productSubSubGroupDto.setSeoSubSubGroup(productSubSubGroup.getSeo());
        if (productSubSubGroup.getImagesubsub() != null) {
            productSubSubGroupDto.setUrlSubSubGroup(productSubSubGroup.getImagesubsub().getOriginalFileName());
        }
        return productSubSubGroupDto;
    }
}
