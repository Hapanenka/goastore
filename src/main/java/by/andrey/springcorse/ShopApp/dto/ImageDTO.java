package by.andrey.springcorse.ShopApp.dto;


import lombok.Data;

@Data
public class ImageDTO {

    private String originalFileName;
}