package by.andrey.springcorse.ShopApp.services;

import by.andrey.springcorse.ShopApp.repositories.OrderRepository;
import by.andrey.springcorse.ShopApp.models.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    private final OrderRepository OrderRepository;

    public OrderService(OrderRepository OrderRepository) {
        this.OrderRepository = OrderRepository;
    }

    public List<Order> findAllOrder() {
        return OrderRepository.findAll();
    }
}