package by.andrey.springcorse.ShopApp.services;

import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.repositories.PeopleRepository;
import by.andrey.springcorse.ShopApp.models.PersonRole;
import by.andrey.springcorse.ShopApp.models.Product;
import by.andrey.springcorse.ShopApp.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDateTime;

@Service
public class RegistrationService {
    private final PeopleRepository peopleRepository;

    private final PasswordEncoder passwordEncoder;

    private final ProductRepository productRepository;

    private final PersonService personService;

    @Autowired
    public RegistrationService(PeopleRepository peopleRepository, PasswordEncoder passwordEncoder,
                               ProductRepository productRepository, PersonService personService) {
        this.peopleRepository = peopleRepository;
        this.passwordEncoder = passwordEncoder;
        this.productRepository = productRepository;
        this.personService = personService;
    }

    @Transactional
    public void register(Person person) {
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        person.setRole(PersonRole.ROLE_USER);
        person.setRegistration(LocalDateTime.now());
        person.setEnabled(true);
        peopleRepository.save(person);
    }

    public void addProduct(Product product) {
        productRepository.save(product);
    }

    @Transactional
    public void updatePerson(Person updatePerson) {         // через мапер не коректно получается. в один метод не выносил
                                                            // потому как update больше ни где не встретится здесь
        Person person = personService.findByName(SecurityContextHolder.getContext().getAuthentication().getName());
        person.setPassword(passwordEncoder.encode(updatePerson.getPassword()));
        person.setUsername(updatePerson.getUsername());
        person.setEmail(updatePerson.getEmail());
        person.setPhone(updatePerson.getPhone());
        person.setRole(updatePerson.getRole());
        person.setDateBirth(updatePerson.getDateBirth());
        person.setCountry(updatePerson.getCountry());
        person.setArea(updatePerson.getArea());
        person.setCity(updatePerson.getCity());
        person.setPostalCode(updatePerson.getPostalCode());
        person.setEnabled(true);
        person.setUpdateDate(LocalDateTime.now());
        peopleRepository.save(person);
    }
}