package by.andrey.springcorse.ShopApp.services;

import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.repositories.PeopleRepository;
import by.andrey.springcorse.ShopApp.util.PersonNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonService {

    private final PeopleRepository peopleRepository;

    public PersonService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    public Person findByName(String username) {
        Optional<Person> person = peopleRepository.findByUsername(username);
        return person.orElseThrow(PersonNotFoundException::new);
    }
}