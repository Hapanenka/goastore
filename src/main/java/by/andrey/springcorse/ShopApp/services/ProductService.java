package by.andrey.springcorse.ShopApp.services;

import by.andrey.springcorse.ShopApp.dto.ProductDTO;
import by.andrey.springcorse.ShopApp.dto.ProductDtoService;
import by.andrey.springcorse.ShopApp.util.ErrorResponse;
import by.andrey.springcorse.ShopApp.util.ProductNotFoundException;
import by.andrey.springcorse.ShopApp.models.Product;
import by.andrey.springcorse.ShopApp.repositories.ProductRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    private final ProductDtoService productDtoService;

    public ProductService(ProductRepository productRepository, ProductDtoService productDtoService) {
        this.productRepository = productRepository;
        this.productDtoService = productDtoService;
    }

    public List<ProductDTO> findAll(boolean sortByCost) {
        if (sortByCost)
            return productRepository.findAll(Sort.by("priceOutComing")).stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
        else
            return productRepository.findAll().stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
    }

    public List<ProductDTO> findWithPagination(Integer page, Integer productPerPage, boolean sortByYear) {
        if (sortByYear)
            return productRepository.findAll(PageRequest.of(page, productPerPage, Sort.by("priceOutComing"))).getContent()
                    .stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
        else
            return productRepository.findAll(PageRequest.of(page, productPerPage)).getContent()
                    .stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
    }

    public ProductDTO findProductById(Long id) {
        return productDtoService.convertToProductDTO(productRepository.findById(id).orElseThrow(ProductNotFoundException::new));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ProductNotFoundException e){
        ErrorResponse response = new ErrorResponse("Product with this id was`t found",
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    public List<ProductDTO> findProductByGroup(Long id) {
        return productRepository.findProductsById(id).stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
    }

    public List<ProductDTO> findProductBySubGroup(Long id) {
        return productRepository.findProductsSubGroupById(id).stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
    }

    public List<ProductDTO> findProductBySubSubGroup(Long id) {
        return productRepository.findProductsSubSubGroupById(id).stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
    }

    public List<ProductDTO> selectRandom(Long quantity) {
        return productRepository.selectRandomProduct(quantity).stream().map(productDtoService::convertToProductDTO).collect(Collectors.toList());
    }
}