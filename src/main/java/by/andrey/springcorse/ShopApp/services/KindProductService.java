package by.andrey.springcorse.ShopApp.services;

import by.andrey.springcorse.ShopApp.dto.ProductGroupDto;
import by.andrey.springcorse.ShopApp.dto.ProductGroupDtoService;
import by.andrey.springcorse.ShopApp.models.*;
import by.andrey.springcorse.ShopApp.repositories.ImageRepository;
import by.andrey.springcorse.ShopApp.repositories.ProductGroupRepository;
import by.andrey.springcorse.ShopApp.repositories.ProductSubGroupRepository;
import by.andrey.springcorse.ShopApp.repositories.ProductSubSubGroupRepository;
import by.andrey.springcorse.ShopApp.util.ProductGroupNotFoundException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class KindProductService {

    private final ProductGroupDtoService productGroupDtoService;

    private final ProductGroupRepository productGroupRepository;

    private final ProductSubGroupRepository productSubGroupRepository;

    private final ProductSubSubGroupRepository productSubSubGroupRepository;

    private final StorageService storageService;

    private final ImageRepository imageRepository;

    public KindProductService(ProductGroupDtoService productGroupDtoService,
                              ProductGroupRepository productGroupRepository, ProductSubGroupRepository productSubGroupRepository,
                              ProductSubSubGroupRepository productSubSubGroupRepository, StorageService storageService,
                              ImageRepository imageRepository) {
        this.productGroupDtoService = productGroupDtoService;
        this.productGroupRepository = productGroupRepository;
        this.productSubGroupRepository = productSubGroupRepository;
        this.productSubSubGroupRepository = productSubSubGroupRepository;
        this.storageService = storageService;
        this.imageRepository = imageRepository;
    }

    public List<ProductGroupDto> findAllGroupProduct(String language) {
        return productGroupRepository.findAll().stream().map(i -> productGroupDtoService.changePgToPgDTO(i, language)).collect(Collectors.toList());
    }

    public List<ProductSubGroup> findAllSubGroupProduct() {
        return productSubGroupRepository.findAll();
    }

    public List<ProductSubSubGroup> findAllSubSubGroupProduct() {
        return productSubSubGroupRepository.findAll();
    }


    public String addGroup(ProductGroup productGroup, MultipartFile file) throws NullPointerException{
       if (file != null){
           productGroup.setImage(storageService.changeFileToImage(file, storageService.uploadFile(file)));
        }
        productGroupRepository.save(productGroup);
        return "You added: " + productGroup;
    }

    @Modifying
    public void groupEdit(long id, ProductGroup pgNew, MultipartFile file){
        ProductGroup pgOld = productGroupRepository.findById(id).orElseThrow(ProductGroupNotFoundException::new);
        if (file != null) {
            storageService.deleteFile(pgOld.getImage().getOriginalFileName().substring(37));
            pgNew.setImage(storageService.changeFileToImage(file, storageService.uploadFile(file)));
            imageRepository.updateById(pgOld.getImage().getIdImage(), pgNew.getImage().getOriginalFileName());
        }
        if (pgNew.getProductGroup() == null){
            pgNew.setProductGroup(pgOld.getProductGroup());
        }
        if (pgNew.getProductGroupRu() == null){
            pgNew.setProductGroupRu(pgOld.getProductGroupRu());
        }
        if (pgNew.getSeo() == null){
            pgNew.setSeo(pgOld.getSeo());
        }
        productGroupRepository.updateById(pgNew.getProductGroup(), pgNew.getProductGroupRu(), pgNew.getSeo(), id);
    }

    public ProductGroupDto findGroupProductById(long id) {
        ProductGroup productGroup = productGroupRepository.findById(id).orElseThrow(ProductGroupNotFoundException::new);
        return productGroupDtoService.changePgToPgDTO(productGroup, "");
    }

    @Modifying
    public void deleteGroupProductById(long id) {
        ProductGroup productGroup = productGroupRepository.findById(id).orElseThrow(ProductGroupNotFoundException::new);
        productGroupRepository.deleteById(id);
        storageService.deleteFile(productGroup.getImage().getOriginalFileName().substring(37));  // delete file from AWS S3 --- by link from DB by name with starting 37 character
    }

    public ProductSubGroup findSubGroupProductById(long id) {
        Optional<ProductSubGroup> foundProductSubGroup = productSubGroupRepository.findById(id);
        return foundProductSubGroup.orElseThrow(ProductGroupNotFoundException::new);
    }

    public void addSubGroup(ProductSubGroup productSubGroup) {
        productSubGroupRepository.save(productSubGroup);
    }

    public void deleteSubGroupProductById(long id) {
        productSubGroupRepository.deleteById(id);
    }

    public ProductSubSubGroup findSubSubGroupProductById(long id) {
        Optional<ProductSubSubGroup> foundProductSubSubGroup = productSubSubGroupRepository.findById(id);
        return foundProductSubSubGroup.orElseThrow(ProductGroupNotFoundException::new);
    }

    public void addSubSubGroup(ProductSubSubGroup productSubSubGroup) {
        productSubSubGroupRepository.save(productSubSubGroup);
    }

    public void deleteSubSubGroupProductById(long id) {
        productSubSubGroupRepository.deleteById(id);
    }

    public void editSubGroup(ProductSubGroup productSubGroup) {
        productSubGroupRepository.saveAndFlush(productSubGroup);
    }

    public void editSubSubGroup(ProductSubSubGroup productSubSubGroup) {
        productSubSubGroupRepository.saveAndFlush(productSubSubGroup);
    }
}