package by.andrey.springcorse.ShopApp.models;

public enum PersonRole {
    ROLE_USER ("ROLE_USER"),
    ROLE_SUPPLIER ("ROLE_SUPPLIER"),
    ROLE_MANAGER ("ROLE_MANAGER"),
    ROLE_REDACTOR ("ROLE_REDACTOR"),
    ROLE_ADMIN ("ROLE_ADMIN"),
    ROLE_OWNER ("ROLE_OWNER");

    private final String displayValue;

    PersonRole(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}