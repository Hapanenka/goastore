package by.andrey.springcorse.ShopApp.models;


import jdk.jfr.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_history")
public class ProductHistory {

    @Id
    @Column(name = "id_product_history")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProductHistory;

    @OneToOne(mappedBy = "productHistory")
    private Product product;

    @Timestamp
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Timestamp
    @Column(name = "mark_delete_date")
    private LocalDateTime markDeleteDate;

    @ManyToOne
    @JoinColumn(name = "who_creation_id", referencedColumnName = "id_person")
    private Person whoCreationId;

    @ManyToOne
    @JoinColumn(name = "who_add_bd_id", referencedColumnName = "id_person")
    private Person whoAddBdId;

    @ManyToOne
    @JoinColumn(name = "who_delete_id", referencedColumnName = "id_person")
    private Person whoDeleteId;
}