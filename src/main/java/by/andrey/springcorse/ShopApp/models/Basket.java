package by.andrey.springcorse.ShopApp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "basket")
public class Basket {

    @Id
    @Column(name = "id_basket")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idBasket;

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "id_order")
    private Order order;

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id_product")
    private Product product;

    @Column(name = "quantity")
    private double quantity;

    @Column(name = "cost")
    private double cost;
}