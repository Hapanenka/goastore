package by.andrey.springcorse.ShopApp.models;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_gr")
public class ProductGroup {

    @Id
    @Column(name = "id_product_group")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProductGroup;

    @JsonIgnore
    @OneToMany(mappedBy = "productGroup", fetch = FetchType.LAZY)
    private List<Product> Products;

    @Column(name = "product_group")
    private String productGroup;

    @Column(name = "product_group_RU")
    private String productGroupRu;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "image_group", referencedColumnName = "id_image")
    private Image image;

    @Column(name = "seo")
    private String seo;

    @OneToMany(mappedBy = "productGroup", fetch = FetchType.LAZY)
    private List<ProductSubGroup> productSubGroups;

    public void setImage(Image image) {
        image.setPreviewImage(true);
        this.image = image;
    }

    public ProductGroup(String productGroup, String productGroupRu, String seo) {
        this.productGroup = productGroup;
        this.productGroupRu = productGroupRu;
        this.seo = seo;
    }
}