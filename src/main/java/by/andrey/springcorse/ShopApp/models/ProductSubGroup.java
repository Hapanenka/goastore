package by.andrey.springcorse.ShopApp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_subgroup")
public class ProductSubGroup {

    @Id
    @Column(name = "id_product_subgroup")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProductSubGroup;

    @JsonIgnore
    @OneToMany(mappedBy = "productSubGroup", fetch = FetchType.LAZY)
    private List<Product> Products;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_group_id", referencedColumnName = "id_product_group")
    private ProductGroup productGroup;

    @Column(name = "product_subgroup")
    private String productSubGroup;

    @Column(name = "product_subgroup_RU")
    private String productSubGroupRu;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_subgroup", referencedColumnName = "id_image")
    private Image imagesub;

    @Column(name = "seo")
    private String seo;

    @OneToMany(mappedBy = "productSubGroup", fetch = FetchType.LAZY)
    private List<ProductSubSubGroup> productSubSubGroups;
}