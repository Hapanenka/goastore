package by.andrey.springcorse.ShopApp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @Column(name = "id_order")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idOrder;

    @OneToMany(mappedBy = "order")
    private List<Basket> baskets;

    @ManyToOne
    @JoinColumn(name = "costumer_id", referencedColumnName = "id_person")
    private Person costumerId;

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "id_person")
    private Person managerId;

    @OneToOne(mappedBy = "order", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private DeliveryOrder idDelivery;

    @OneToOne(mappedBy = "order", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private HistoryOrder idHistory;
}