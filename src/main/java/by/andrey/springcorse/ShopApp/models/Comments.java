package by.andrey.springcorse.ShopApp.models;

import jdk.jfr.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "comments")
public class Comments {

    @Id
    @Column(name = "id_comment")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idComment;

    @ManyToOne
    @JoinColumn(name = "product_commented_id", referencedColumnName = "id_product")
    private Product product;

    @Column(name = "text_comment")
    private String textComment;

    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "id_person")
    private Person author;

    @Column(name = "add_date")
    @Timestamp
    private LocalDateTime updateDate;
}