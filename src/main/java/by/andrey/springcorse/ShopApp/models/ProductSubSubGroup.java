package by.andrey.springcorse.ShopApp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_sub_subgroup")
public class ProductSubSubGroup {

    @Id
    @Column(name = "id_product_sub_subgroup")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProductSubSubGroup;

    @JsonIgnore
    @OneToMany(mappedBy = "productSubSubGroup", fetch = FetchType.LAZY)
    private List<Product> Products;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_subgroup_id", referencedColumnName = "id_product_subgroup")
    private ProductSubGroup productSubGroup;

    @Column(name = "product_sub_subgroup")
    private String productSubSubGroup;

    @Column(name = "product_sub_subgroup_RU")
    private String productSubSubGroupRu;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_sub_subgroup", referencedColumnName = "id_image")
    private Image imagesubsub;

    @Column(name = "seo")
    private String seo;
}