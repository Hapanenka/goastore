package by.andrey.springcorse.ShopApp.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "image")
public class Image implements Serializable {

    @Id
    @Column(name = "id_image")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idImage;

    @JsonBackReference
    @ManyToMany(mappedBy = "images")
    private List<Product> products;

    @Column(name = "name_image")
    private String name;

    @Column(name = "original_file_name")
    private String originalFileName;

    @Column(name = "size")
    private Long size;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "is_preview_image")
    private boolean isPreviewImage;

    @OneToOne(mappedBy = "image")
    @JsonBackReference
    private ProductGroup productGroup;

    @OneToOne(mappedBy = "imagesub")
    @JsonBackReference
    private ProductSubGroup productSubGroup;

    @OneToOne(mappedBy = "imagesubsub")
    @JsonBackReference
    private ProductSubSubGroup productSubSubGroup;
}