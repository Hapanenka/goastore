package by.andrey.springcorse.ShopApp.models;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product implements Serializable {

    @Id
    @Column(name = "id_product")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProduct;

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private List<Basket> baskets;

    @JsonManagedReference
    @ManyToMany
    @JoinTable(
            name = "product_image",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "image_id"))
    private List<Image> images;

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private List<Comments> comments;

    @Column(name = "index")     //внутренние индексы магазина (маркировка) м.б. и буквы и цифры
    private String index;           //а может и не нужен обойтись ID только

    @NotEmpty
    @Column(name = "product_name")
    private String productName;

    @NotEmpty
    @Column(name = "product_name_RU")
    private String productNameRu;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sub_sub_product_group_id", referencedColumnName = "id_product_sub_subgroup")
    private ProductSubSubGroup productSubSubGroup;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sub_product_group_id", referencedColumnName = "id_product_subgroup")
    private ProductSubGroup productSubGroup;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_group_id", referencedColumnName = "id_product_group")
    private ProductGroup productGroup;

    @Column(name = "price_incoming")
    private double priceInComing;

    @Column(name = "price_outcoming")
    private double priceOutComing;

    @Column(name = "price_without_discount")
    private double priceWithOutDiscount;

    @Column(name = "product_discount")
    private double productDiscount;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "product_history_id", referencedColumnName = "id_product_history")
    private ProductHistory productHistory;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "maker", referencedColumnName = "id_maker")
    private Maker maker;

    @Column(name = "product_status")
    private String productStatus;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "specifications", referencedColumnName = "id_specification")
    private Specification specification;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rating", referencedColumnName = "id_rating")
    @JsonManagedReference
    private Rating rating;
}