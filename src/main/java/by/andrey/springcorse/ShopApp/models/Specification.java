package by.andrey.springcorse.ShopApp.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "specification")
public class Specification {

    @Id
    @Column(name = "id_specification")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idSpecification;

    @OneToOne(mappedBy = "specification")
    @JsonBackReference
    private Product product;

    @Column(name = "weight_netto")
    private double weightNetto;

    @Column(name = "weight_brutto")
    private double weightBrutto;

    @Column(name = "color")
    private String color;           // Может быть Enum или Таблицой реализовать...

    @Column(name = "size")
    private double size;

    @Column(name = "material")
    private String material;



    // МНОГО И МНОГО ПАРАМЕТРОВ НУЖНО ДОБАВЛЯТЬ, ПРОДУМЫВАТЬ

}
