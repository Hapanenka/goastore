package by.andrey.springcorse.ShopApp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "delivery")
public class DeliveryOrder {

    @Id
    @Column(name = "id_delivery")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDelivery;

    @OneToOne                                                   //NEED TESTS
    @MapsId
    @JoinColumn(name = "id_delivery")
    private Order order;

    @Column(name = "postal_code")
    private int postalCode;

    @Column(name = "country")
    private String country;

    @Column(name = "area")
    private String area;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "house")
    private String house;

    @Column(name = "flat")
    private String flat;

    @Column(name = "phone")
    private String phone;

    @Column(name = "other_info")
    private String otherInfo;
}