package by.andrey.springcorse.ShopApp.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "maker")
public class Maker {

    @Id
    @Column(name = "id_maker")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idProductGroup;

    @Column(name = "name_maker")
    private String nameMaker;

    @Column(name = "country_maker")
    private String countryMaker;

    @Column(name = "url_maker_logo")
    private String urlMakerLogo;

    @Column(name = "url_maker_info")
    private String urlMakerInfo;

    @Column(name = "url_maker_info_RU")
    private String urlMakerInfoRu;

    @OneToMany(mappedBy = "maker")
    @JsonBackReference
    private List<Product> product;
}