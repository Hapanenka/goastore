package by.andrey.springcorse.ShopApp.models;


import jdk.jfr.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "person")
public class Person {

    @Id
    @Column(name = "id_person")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPerson;

    @Column(name = "user_name", unique = true)
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_birth")
    @Timestamp
    private LocalDateTime dateBirth;

    @Column(name = "phone", length = 20)
    private String phone;

    @Column(name = "email", unique = true, length = 35)
    private String email;

    @Column(name = "password", length = 100)
    private String password;

    @Column(name = "city")
    private String city;

    @Column(name = "area")
    private String area;

    @Column(name = "postal_code")
    private int postalCode;

    @Column(name = "country")
    private String country;

    @Column(name = "person_role", length = 10)
    @Enumerated(EnumType.STRING)
    private PersonRole role;

    @Column(name = "registration")
    @Timestamp
    private LocalDateTime registration;

    @Column(name = "update_date")
    @Timestamp
    private LocalDateTime updateDate;

    @Column(name = "is_enabled")
    private boolean isEnabled = true;

    @Column(name = "other_info")
    private String otherInfo;

    @Column(name = "other_info_RU")
    private String otherInfoRu;

    @Column(name = "url_person_avatar")
    private String urlPersonAvatar;

    @Column(name = "personal_discount")
    private int personalDiscount;

    @OneToMany(mappedBy = "costumerId")
    private List<Order> costumerOrder;

    @OneToMany(mappedBy = "author")
    private List<Comments> comments;

    @OneToMany(mappedBy = "managerId")
    private List<Order> costumerManager;

    @OneToMany(mappedBy = "whoCreationId")
    private List<ProductHistory> whoCreationProduct;

    @OneToMany(mappedBy = "whoAddBdId")
    private List<ProductHistory> whoAddBdProduct;

    @OneToMany(mappedBy = "whoDeleteId")
    private List<ProductHistory>whoDeleteProduct;
}