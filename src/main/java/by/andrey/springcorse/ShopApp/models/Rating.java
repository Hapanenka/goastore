package by.andrey.springcorse.ShopApp.models;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ratings")
public class Rating {

    @Id
    @Column(name = "id_rating")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idRating;

    @OneToOne(mappedBy = "rating")
    @JsonBackReference
    private Product product;

    @Column(name = "current_rating")
    private double currentRating;

    @Column(name = "number_of_rating")
    private int numberOfRatings;
}