package by.andrey.springcorse.ShopApp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "history_order")
public class HistoryOrder {

    @Id
    @Column(name = "id_history")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idHistory;

    @OneToOne                                                   //NEED TESTS
    @MapsId
    @JoinColumn(name = "id_history")
    private Order order;

    @Column(name = "add_date")
    private Date addDate;

    @Column(name = "payment_confirmation")
    private Date paymentConfirmation;

    @Column(name = "sending_confirmation")
    private Date sendingConfirmation;

    @Column(name = "supply_confirmation")
    private Date supplyConfirmation;
}