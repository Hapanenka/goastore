package by.andrey.springcorse.ShopApp.validators;

import by.andrey.springcorse.ShopApp.models.ProductGroup;
import by.andrey.springcorse.ShopApp.models.ProductSubGroup;
import by.andrey.springcorse.ShopApp.repositories.ProductSubGroupRepository;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ValidatorProductSubGroup implements Validator {

    private final ProductSubGroupRepository productSubGroupRepository;

    public ValidatorProductSubGroup(ProductSubGroupRepository productSubGroupRepository) {
        this.productSubGroupRepository = productSubGroupRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProductSubGroup productSubGroup = (ProductSubGroup) target;
        if(productSubGroupRepository.findByProductSubGroupOrProductSubGroupRu(productSubGroup.getProductSubGroup(),
                productSubGroup.getProductSubGroupRu()).isPresent()) {
            errors.rejectValue("ProductSubGroup", "", "This product group is already registered");
        }
    }
}