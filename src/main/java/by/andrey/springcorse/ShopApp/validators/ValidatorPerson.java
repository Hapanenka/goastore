package by.andrey.springcorse.ShopApp.validators;

import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.repositories.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ValidatorPerson implements Validator {

    private final PeopleRepository peopleRepository;

    public ValidatorPerson(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Person person = (Person) target;
        if(peopleRepository.findByUsername(person.getUsername()).isPresent()) {
           errors.rejectValue("username", "", "This data is busy");
        }
    }
}