package by.andrey.springcorse.ShopApp.validators;

import by.andrey.springcorse.ShopApp.models.ProductSubGroup;
import by.andrey.springcorse.ShopApp.models.ProductSubSubGroup;
import by.andrey.springcorse.ShopApp.repositories.ProductSubSubGroupRepository;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ValidatorProductSubSubGroup implements Validator {

    private final ProductSubSubGroupRepository productSubSubGroupRepository;

    public ValidatorProductSubSubGroup(ProductSubSubGroupRepository productSubSubGroupRepository) {
        this.productSubSubGroupRepository = productSubSubGroupRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProductSubSubGroup productSubSubGroup = (ProductSubSubGroup) target;
        if(productSubSubGroupRepository.findByProductSubSubGroupOrProductSubSubGroupRu(productSubSubGroup.getProductSubSubGroup(),
                productSubSubGroup.getProductSubSubGroupRu()).isPresent()) {
            errors.rejectValue("ProductSubSubGroup", "", "This product group is already registered");
        }
    }
}