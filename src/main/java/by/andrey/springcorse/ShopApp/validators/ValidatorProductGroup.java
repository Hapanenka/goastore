package by.andrey.springcorse.ShopApp.validators;

import by.andrey.springcorse.ShopApp.dto.ProductGroupDto;
import by.andrey.springcorse.ShopApp.dto.ProductGroupDtoService;
import by.andrey.springcorse.ShopApp.models.ProductGroup;
import by.andrey.springcorse.ShopApp.repositories.ProductGroupRepository;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ValidatorProductGroup implements Validator {

    private final ProductGroupRepository productGroupRepository;

    public ValidatorProductGroup(ProductGroupRepository productGroupRepository) {
        this.productGroupRepository = productGroupRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProductGroup productGroup = (ProductGroup) target;
        if(productGroupRepository.findByProductGroupOrProductGroupRu(productGroup.getProductGroup(),
                productGroup.getProductGroupRu()).isPresent()) {
            errors.reject("", "This data is busy");
        }
    }
}