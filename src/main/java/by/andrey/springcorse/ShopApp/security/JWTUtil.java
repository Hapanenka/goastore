package by.andrey.springcorse.ShopApp.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;


import java.util.Date;

@Component
public class JWTUtil {

    @Value("${jwt_secret}")
    private String secret;

    @Value("${app.jwtExpirationMs}")
    private int jwtExpirationMs;

  public String generationToken(Authentication authentication) {

      PersonDetails userPrincipal = (PersonDetails) authentication.getPrincipal();

      return Jwts.builder().setSubject((userPrincipal.getUsername())).setIssuedAt(new Date())
              .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
              .signWith(SignatureAlgorithm.HS512, secret).compact();
  }

    public boolean validateJwtToken(String jwt) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(jwt);
            return true;
        } catch (MalformedJwtException | IllegalArgumentException e) {
            new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
        return false;
    }

   public String getUserNameFromJwtToken(String jwt) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(jwt).getBody().getSubject();
    }
}