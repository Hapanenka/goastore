package by.andrey.springcorse.ShopApp.security;

import lombok.AllArgsConstructor;
import lombok.Data;


import java.util.List;

@Data
public class JwtResponse {

    private String token;

    private String type = "Bearer";

    private Long id;

    private String email;


    private String userName;

    private String role;

    public JwtResponse(String token, Long id, String email, String userName, String role) {
        this.token = token;
        this.id = id;
        this.email = email;
        this.userName = userName;
        this.role = role;
    }
}