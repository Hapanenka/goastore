package by.andrey.springcorse.ShopApp.repositories;

import by.andrey.springcorse.ShopApp.models.ProductSubGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductSubGroupRepository extends JpaRepository<ProductSubGroup, Long>{
    Optional<Object> findByProductSubGroupOrProductSubGroupRu(String productSubGroup, String productSubGroupRu);
}
