package by.andrey.springcorse.ShopApp.repositories;


import by.andrey.springcorse.ShopApp.models.ProductSubSubGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductSubSubGroupRepository extends JpaRepository<ProductSubSubGroup, Long> {
    Optional<Object> findByProductSubSubGroupOrProductSubSubGroupRu(String productSubSubGroup, String productSubSubGroupRu);
}