package by.andrey.springcorse.ShopApp.repositories;


import by.andrey.springcorse.ShopApp.models.Product;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(value = "Select * from product where type_product=?1", nativeQuery = true)
    List<Product> findProducts(Long id);

    @Query(value = "Select * from product where product_group_id=?1", nativeQuery = true)
    List<Product> findProductsById(Long id);

    @Query(value = "Select * from product where sub_product_group_id=?1", nativeQuery = true)
    List<Product> findProductsSubGroupById(Long id);

    @Query(value = "Select * from product where sub_sub_product_group_id=?1", nativeQuery = true)
    List<Product> findProductsSubSubGroupById(Long id);

    @Query(value = "select * from product order by random() limit ?1", nativeQuery = true)
    List<Product> selectRandomProduct(Long quantity);
}