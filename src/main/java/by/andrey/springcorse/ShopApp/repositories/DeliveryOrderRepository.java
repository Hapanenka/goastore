package by.andrey.springcorse.ShopApp.repositories;


import by.andrey.springcorse.ShopApp.models.DeliveryOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryOrderRepository extends JpaRepository<DeliveryOrder, Long> {
}
