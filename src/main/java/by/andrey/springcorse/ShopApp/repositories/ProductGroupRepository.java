package by.andrey.springcorse.ShopApp.repositories;

import by.andrey.springcorse.ShopApp.models.Image;
import by.andrey.springcorse.ShopApp.models.ProductGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface ProductGroupRepository extends JpaRepository<ProductGroup, Long> {
    Optional<ProductGroup> findByProductGroupOrProductGroupRu(String productGroup, String productGroupRu);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE product_gr SET product_group = ?1, product_group_ru = ?2, seo = ?3 WHERE id_product_group = ?4",
            nativeQuery = true)
    void updateById(String productGroup, String productGroupRu, String seo, long id);
}