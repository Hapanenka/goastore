package by.andrey.springcorse.ShopApp.repositories;

import by.andrey.springcorse.ShopApp.models.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface ImageRepository extends JpaRepository<Image, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE image SET original_file_name = ?2 WHERE id_image = ?1",
            nativeQuery = true)
    void updateById(long idImage, String originalFileName);
}
