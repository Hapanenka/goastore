package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.services.OrderService;
import by.andrey.springcorse.ShopApp.models.Order;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/admin")
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@CrossOrigin(origins = "http://localhost:8091", maxAge = 3600)
public class  AdminController {

    private final OrderService OrderService;

    public AdminController(by.andrey.springcorse.ShopApp.services.OrderService orderService) {
        OrderService = orderService;
    }

    @GetMapping("/order")
    public List<Order> getOrder() {
        return OrderService.findAllOrder();
    }
}