package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.dto.ProductGroupDto;
import by.andrey.springcorse.ShopApp.models.ProductGroup;
import by.andrey.springcorse.ShopApp.models.ProductSubGroup;
import by.andrey.springcorse.ShopApp.models.ProductSubSubGroup;
import by.andrey.springcorse.ShopApp.services.KindProductService;
import by.andrey.springcorse.ShopApp.util.ErrorResponse;
import by.andrey.springcorse.ShopApp.util.ExceptionReg;
import by.andrey.springcorse.ShopApp.util.NotCreatedException;
import by.andrey.springcorse.ShopApp.util.ProductGroupNotFoundException;
import by.andrey.springcorse.ShopApp.validators.ValidatorProductGroup;
import by.andrey.springcorse.ShopApp.validators.ValidatorProductSubGroup;
import by.andrey.springcorse.ShopApp.validators.ValidatorProductSubSubGroup;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
@CrossOrigin(origins = "http://localhost:8091", maxAge = 3600)
public class KindController {

    private final KindProductService kindProductService;

    private final ValidatorProductGroup validatorProductGroup;

    private final ValidatorProductSubGroup validatorProductSubGroup;

    private final ValidatorProductSubSubGroup validatorProductSubSubGroup;

    private final ExceptionReg exceptionReg;

    public KindController(KindProductService kindProductService, ValidatorProductGroup validatorProductGroup,
                          ValidatorProductSubGroup validatorProductSubGroup, ValidatorProductSubSubGroup validatorProductSubSubGroup,
                          ExceptionReg exceptionReg) {
        this.kindProductService = kindProductService;
        this.validatorProductGroup = validatorProductGroup;
        this.validatorProductSubGroup = validatorProductSubGroup;
        this.validatorProductSubSubGroup = validatorProductSubSubGroup;
        this.exceptionReg = exceptionReg;
    }
    /////////////////////////////GROUPS/////////////////////////////////////

    @GetMapping
    public ResponseEntity<List<ProductGroupDto>> checkAllKindsProduct(@RequestParam(value = "language", defaultValue = "eng") String language) {
        return ResponseEntity
                .ok()
                .body(kindProductService.findAllGroupProduct(language));
    }

    @GetMapping("/group/{id}")
    public ResponseEntity<ProductGroupDto> checkGroupProduct(@PathVariable("id") int id) {
        return ResponseEntity
                .ok()
                .body(kindProductService.findGroupProductById(id));
    }

    @PostMapping(path = "/groupAdd")
    @ResponseStatus(value = HttpStatus.CREATED)
    public String addGroup(@RequestPart String productGroup,
                           @RequestPart String productGroupRu,
                           @RequestPart String seo,
                           BindingResult bindingResult,
                           MultipartFile file) {
        ProductGroup newProductGroup = new ProductGroup(productGroup, productGroupRu, seo);
        validatorProductGroup.validate(newProductGroup, bindingResult);
        exceptionReg.except(bindingResult);
        return kindProductService.addGroup(newProductGroup, file);
    }

    @PostMapping("/groupEdit/{id}")
    public ResponseEntity<HttpStatus> editProductGroup(
            @PathVariable("id") int id,
            @Nullable MultipartFile file,
            @RequestPart @Nullable String productGroup,
            @RequestPart @Nullable String productGroupRu,
            @RequestPart @Nullable String seo,
            BindingResult bindingResult) {

        ProductGroup newProductGroup = new ProductGroup(productGroup, productGroupRu, seo);

        if (productGroup != null || productGroupRu != null) {

            validatorProductGroup.validate(newProductGroup, bindingResult);
            exceptionReg.except(bindingResult);
        }
        kindProductService.groupEdit(id, newProductGroup, file);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/deleteGroup/{id}")
    public ResponseEntity<HttpStatus> deleteGroupProduct(@PathVariable("id") int id) {
        kindProductService.deleteGroupProductById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

/////////////////////////////////////SUBGROUPS////////////////////////////////////////////

    @GetMapping("/subgroups")
    public ResponseEntity<List<ProductSubGroup>> checkAllSubGroupProduct() {
        return ResponseEntity
                .ok()
                .body(kindProductService.findAllSubGroupProduct());
    }

    @GetMapping("/subgroup/{id}")
    public ProductSubGroup checkSubGroupProduct(@PathVariable("id") int id) {
        return kindProductService.findSubGroupProductById(id);
    }

    @PostMapping("/subgroupAdd")
    public ResponseEntity<HttpStatus> addSubGroup(@RequestBody @Valid ProductSubGroup productSubGroup, BindingResult bindingResult) {
        validatorProductSubGroup.validate(productSubGroup, bindingResult);
        exceptionReg.except(bindingResult);
        kindProductService.addSubGroup(productSubGroup);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/subgroupEdit")
    public ResponseEntity<HttpStatus> editProductSubGroup(@RequestBody @Valid ProductSubGroup productSubGroup,
                                                          BindingResult bindingResult) {
        exceptionReg.except(bindingResult);
        kindProductService.editSubGroup(productSubGroup);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/deleteSubGroup/{id}")
    public ResponseEntity<HttpStatus> deleteSubGroupProduct(@PathVariable("id") int id) {
        kindProductService.deleteSubGroupProductById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

//////////////////////////////////SUB-SUBGROUPS//////////////////////////////////////////////////////

    @GetMapping("/subSubgroups")
    public List<ProductSubSubGroup> checkAllSubSubGroupProduct() {
        return kindProductService.findAllSubSubGroupProduct();
    }

    @GetMapping("/subSubgroup/{id}")
    public ProductSubSubGroup checkSubSubGroupProduct(@PathVariable("id") int id) {
        return kindProductService.findSubSubGroupProductById(id);
    }

    @PostMapping("/subSubgroupAdd")
    public ResponseEntity<HttpStatus> addSubSubGroup(@RequestBody @Valid ProductSubSubGroup productSubSubGroup,
                                                     BindingResult bindingResult) {
        validatorProductSubSubGroup.validate(productSubSubGroup, bindingResult);
        exceptionReg.except(bindingResult);
        kindProductService.addSubSubGroup(productSubSubGroup);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/subSubgroupEdit")
    public ResponseEntity<HttpStatus> editProductSubSubGroup(@RequestBody @Valid ProductSubSubGroup productSubSubGroup,
                                                             BindingResult bindingResult) {
        exceptionReg.except(bindingResult);
        kindProductService.editSubSubGroup(productSubSubGroup);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/deleteSubSubGroup/{id}")
    public ResponseEntity<HttpStatus> deleteSubSubGroupProduct(@PathVariable("id") int id) {
        kindProductService.deleteSubSubGroupProductById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedException e) {
        ErrorResponse response = new ErrorResponse(e.getMessage(),
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ProductGroupNotFoundException e) {
        ErrorResponse response = new ErrorResponse(e.getMessage(),
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}