package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.dto.PersonDTO;
import by.andrey.springcorse.ShopApp.dto.PersonDtoService;
import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.services.*;
import by.andrey.springcorse.ShopApp.util.ExceptionReg;
import by.andrey.springcorse.ShopApp.validators.ValidatorPerson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:8091", maxAge = 3600)
@RestController
@RequestMapping
public class PersonController {

    private final AdminService adminService;

    private final PersonService personService;

    private final RegistrationService registrationService;

    private final ExceptionReg exceptionReg;

    private final PersonDtoService personDtoService;

    private final ValidatorPerson validatorPerson;

    public PersonController(AdminService adminService, PersonService personService, RegistrationService registrationService,
                            ExceptionReg exceptionReg, PersonDtoService personDtoService, ValidatorPerson validatorPerson) {
        this.adminService = adminService;
        this.personService = personService;
        this.registrationService = registrationService;
        this.exceptionReg = exceptionReg;
        this.personDtoService = personDtoService;
        this.validatorPerson = validatorPerson;
    }

    @GetMapping("/profile")
    public PersonDTO profile(Principal principal) {
        Person person = new Person();
        try {
            person = personService.findByName(principal.getName());
        } catch (NullPointerException e) {
        }
        return personDtoService.convertToPersonDTO(person);
    }

    @PostMapping("/profile")
    public ResponseEntity<HttpStatus> updatePersonDto(@RequestBody @Valid PersonDTO personDTO,
                                                      BindingResult bindingResult, Principal principal) {
        if (principal == null || !Objects.equals(principal.getName(), personDTO.getUserName())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else
        validatorPerson.validate(personDtoService.convertToPerson(personDTO), bindingResult);
        exceptionReg.except(bindingResult);
        registrationService.updatePerson(personDtoService.convertToPerson(personDTO));
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/checkPerson/{id}")
    public PersonDTO getPerson(@PathVariable("id") int id) {
        return personDtoService.convertToPersonDTO(adminService.findById(id));
    }

    @DeleteMapping("/deletePerson/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable("id") int id){
        adminService.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/checkAllPeople")                // ТЕСТ метод   --- after DELETE
    public List<PersonDTO> adminCheck(){
        return adminService.findAll()
                .stream()
                .map(personDtoService::convertToPersonDTO)
                .collect(Collectors.toList());
    }
}