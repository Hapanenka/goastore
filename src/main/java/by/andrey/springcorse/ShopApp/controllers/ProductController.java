package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.dto.ProductDTO;
import by.andrey.springcorse.ShopApp.dto.ProductDtoService;
import by.andrey.springcorse.ShopApp.models.Product;
import by.andrey.springcorse.ShopApp.services.ProductService;
import by.andrey.springcorse.ShopApp.services.RegistrationService;
import by.andrey.springcorse.ShopApp.util.ExceptionReg;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "http://localhost:8091", maxAge = 3600)
public class ProductController {

    private final ProductService productService;

    private final ExceptionReg exceptionReg;

    private final RegistrationService registrationService;

    private final ProductDtoService productDtoService;

    public ProductController(ProductService productService, ExceptionReg exceptionReg, RegistrationService
            registrationService, ProductDtoService productDtoService) {
        this.productService = productService;
        this.exceptionReg = exceptionReg;
        this.registrationService = registrationService;
        this.productDtoService = productDtoService;
    }

    @GetMapping()
    public ResponseEntity<List<ProductDTO>> index(@RequestParam(value = "page", required = false) Integer page,
                        @RequestParam(value = "product_per_page", required = false) Integer productPerPage,
                        @RequestParam(value = "priceOutComing", required = false) boolean sortByCost) {
        if (page == null || productPerPage == null)
            return ResponseEntity.ok()
                    .body(productService.findAll(sortByCost));
        else
           return ResponseEntity.ok()
                   .body(productService.findWithPagination(page, productPerPage, sortByCost));
    }

    @GetMapping("/random/{quantity}")
    public ResponseEntity<List<ProductDTO>> checkRandomProd(@PathVariable Long quantity) {
        return ResponseEntity
                .ok()
                .body(productService.selectRandom(quantity));
    }

    @GetMapping("/group/{id}")
    public ResponseEntity<List<ProductDTO>> checkProductGroupById(@PathVariable Long id) {
        return ResponseEntity
                .ok()
                .body(productService.findProductByGroup(id));
    }

    @GetMapping("/subgroup/{id}")
    public ResponseEntity<List<ProductDTO>> checkProductSubGroupById(@PathVariable Long id) {
        return ResponseEntity
                .ok()
                .body(productService.findProductBySubGroup(id));
    }

    @GetMapping("/subsubgroup/{id}")
    public ResponseEntity<List<ProductDTO>> checkProductSubSubGroupById(@PathVariable Long id) {
        return ResponseEntity
                .ok()
                .body(productService.findProductBySubSubGroup(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> checkProductById(@PathVariable Long id) {
        return ResponseEntity
                .ok()
                .body(productService.findProductById(id));
    }

    @PostMapping("/addProduct")
    public ResponseEntity<HttpStatus> addProduct(@RequestBody @Valid ProductDTO productDTO, BindingResult bindingResult){
        exceptionReg.except(bindingResult);
        registrationService.addProduct(productDtoService.convertToProduct(productDTO));
        return ResponseEntity.ok(HttpStatus.OK);
    }
}