package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.dto.AuthenticationDTO;
import by.andrey.springcorse.ShopApp.dto.PersonDTO;
import by.andrey.springcorse.ShopApp.dto.PersonDtoService;
import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.security.JWTUtil;
import by.andrey.springcorse.ShopApp.security.JwtResponse;
import by.andrey.springcorse.ShopApp.security.PersonDetails;
import by.andrey.springcorse.ShopApp.services.RegistrationService;
import by.andrey.springcorse.ShopApp.util.*;
import by.andrey.springcorse.ShopApp.validators.ValidatorPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "http://localhost:8091", maxAge = 3600)
public class AuthController {

    private final RegistrationService registrationService;

    private final ExceptionReg exceptionReg;

    private final ValidatorPerson validatorPerson;

    private final JWTUtil jwtUtil;

    private final PersonDtoService personDtoService;

    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthController(RegistrationService registrationService, ExceptionReg exceptionReg,
                          ValidatorPerson validatorPerson, JWTUtil jwtUtil, PersonDtoService personDtoService,
                          AuthenticationManager authenticationManager) {
        this.registrationService = registrationService;
        this.exceptionReg = exceptionReg;
        this.validatorPerson = validatorPerson;
        this.jwtUtil = jwtUtil;
        this.personDtoService = personDtoService;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/show")        // test method
    public String showUserName(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping("/registration")
    public ResponseEntity<HttpStatus> create(@RequestBody @Valid PersonDTO personDTO, BindingResult bindingResult) {
        Person person = personDtoService.convertToPerson(personDTO);
        validatorPerson.validate(person, bindingResult);
        exceptionReg.except(bindingResult);
        registrationService.register(person);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<?> authUser(@RequestBody AuthenticationDTO authenticationDTO) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        authenticationDTO.getUserName(),
                        authenticationDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtil.generationToken(authentication);
        PersonDetails userDetails = (PersonDetails) authentication.getPrincipal();
        String role = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).toList().toString();
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                role));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedException e) {
        ErrorResponse response = new ErrorResponse(e.getMessage(),
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(PersonNotFoundException e) {
        ErrorResponse response = new ErrorResponse("Person with this id was`t found",
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}